To start the server:

On the command line, navigate to MileageCalculator/server
Run the command “node script.js”
Visit http://localhost:1334/employees in a browser to ensure that the server is up and running (expected output: {"Rachel Smith":"59 Bow Street, Arlington, MA","Donald Palmer":"151 Shade Street, Lexington, MA"} )

To run the application:
 
Visit MileageCalculator/calculator.html in a browser. 