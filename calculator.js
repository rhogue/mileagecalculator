var app= angular.module('mileageCalculator', []);

// var employeeData = `{
// 	"Rachel Smith" : "59 Bow Street, Arlington, MA",
// 	"Donald Palmer" : "151 Shade Street, Lexington, MA"
// }`;

var max_locations = 4;

app.controller('formControl', function($scope){

	function reqListener () {
		$scope.employees = JSON.parse(this.responseText);
		$scope.$apply();
	}

	var req = new XMLHttpRequest();
	req.addEventListener("load", reqListener);
	req.open("GET", "http://localhost:1334/employees");
	req.send();

	$scope.locations = [];
	//initialize to empty string
	for(var i=0; i < max_locations; i++) {
		$scope.locations[i] = "";
	}

	$scope.calculate_mileage = function() {
		if(($scope.user != null) && ($scope.user.name != null) && ($scope.user.name.length > 0) && ($scope.locations[0].length > 0)) {
			var service = new google.maps.DirectionsService();
			var waypoints = [];
			var destinations = $scope.locations.filter(Boolean); // remove empty addresses
			for (dest in destinations) {
				waypoints.push({location: destinations[dest], stopover: false});
			}
			var request = {
	    	origin: $scope.employees[$scope.user.name],
	    	destination: $scope.employees[$scope.user.name],
	    	waypoints: waypoints,
	    	travelMode: 'DRIVING',
	    	provideRouteAlternatives: false,
	    	unitSystem: google.maps.UnitSystem.IMPERIAL
	  	};
	  	service.route(request, function(result, status) {
		    if (status !== 'OK') {
		      alert('Error was: ' + status);
		    } else {
		    	var mileage = result.routes[0].legs[0].distance.text;
		    	var outputDiv = document.getElementById('mileage');
		    	outputDiv.innerHTML = mileage;
		    }
	  	});
	  } else {
	  	if ($scope.locations[0].length <= 0) {
	  		alert("Please specify at least one location");
	  	} else {
	  		alert("Please select an employee.");
	  	}
	  }
	}
});