var express = require('express');
var app = express();
var cors = require('cors');
app.use(cors());

app.get('/employees', (req, res) => {
	var json = require('./employee_data.json');
	res.json(json);
})

app.listen(1334);